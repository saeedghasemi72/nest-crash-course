import { Comment } from './src/entities/comment.entity';
import { Topic } from './src/entities/topic.entity';
import { User } from './src/entities/user.entity';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';

const config: PostgresConnectionOptions = {
  type: 'postgres',
  database: 'testDB',
  host: 'localhost',
  username: 'postgres',
  password: 'postgres',
  port: 5432,
  entities: [User, Topic, Comment],
  synchronize: true,
  logging: true,
};

export default config;
